/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
    2. Create a class constructor able to receive 3 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.



	Create 2 new objects using our class constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.


	Pushing Instructions:

	Create a git repository named S24.
	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	Add the link in Boodle.

*/

//Solution: 

/*Debug*/
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}


let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}


/*Debug and Update*/

function introduce(student){

	// console.log("Hi! I'm " + student.names + "." + "I am " + student.ages + " years old.")
	// console.log("I study the following courses: " + student.class)

	console.log(`Hi I'm ${student.name}. I am ${student.age} yaers old.`)
	console.log(`I study the following courses: ${student.classes}`)

}

introduce(student1);
introduce(student2);




// function getCube(num){

// 	console.log(Math.pow(num,3));

// }

getCube = (num) => {

	console.log("for getCube:")
	console.log(num **3);

}

getCube(6)




let numArr = [15,16,32,21,21,2]

// numArr.forEach(function(num){
// 	console.log(num);
// })


numArr.forEach (num => 
	console.log(num))


// let numsSquared = numArr.map(function(num){

// 	return number ** 2;

//   }
// )

let numsSquared = numArr.map(num =>{

	return num ** 2;

  }
)

console.log(numsSquared);


/*2. Class Constructor*/

class Chartacter{
	constructor(username, role, guildName, level){

		this.username = username;
		this.role = role;
		this.guildName = guildName;
		this.level = level;

	}
}

let Ashe = new Chartacter("Ashe123", "Marksman", "Beginner", 6);
let MasterYi = new Chartacter("Mr. Yeah", "Body Bag", "Cowsep", 10);
let Senna = new Chartacter("Senna_ALL", "Support Killer", "Not a guild", 10000000);

console.log(Ashe);
console.log(MasterYi);
console.log(Senna);

class Food{
	constructor(taste, color, chef, price){

		this.taste = taste;
		this.color = color;
		this.chef = chef;
		this.price = price;

	}
}

let sinigang = new Food("Sour", "Orange", "Boy Logro", 500);
let adobo = new Food("Masarap", "Brownish", "Mother dear", 0);
let sunnySideUp = new Food("Meeh", "Black", "Me", 10000000);

console.log(sinigang);
console.log(adobo);
console.log(sunnySideUp);