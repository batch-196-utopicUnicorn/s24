

//ES6 Updates
//ES6 is one of the latest version of writing Javascript and in fact is one of the largest major update to JS.
//let, const - are ES6 updates are the new standarts of creating variables
//var - was the keyword to create variables before ES6;

// console.log(varSample);
// var varSample = "Hoist me up!";

//Exponent Operator
let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);
//Math.pow() allows us to get the result of a number raised to a given exponent.

//Math.pow(base, exponent)
//Exponent Operators ** - allows us to get the result of a number raised to a given exponent. It is used as an alternative to Math.pow()
let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);//25

let fivePowerOf4 = 5**4;
console.log(fivePowerOf4);

let squareRootOf4 = 4**.5;
console.log(squareRootOf4);//2

let string1 = "Javascript";
let string2 = "not";
let string3 = "is";
let string4 = "TypeScript";
let string5 = "Java";
let string6 = "Zuitt";
let string7 = "Coding";

//Template Literals
//"", '' - string literals
//Template Literals allows us to create strings using `` and easily embed JS expression in it.

let sentence1 = `${string1} ${string3} ${string2} ${string5}`;
let sentence2 = `${string4} ${string3} ${string1}`;

console.log(sentence1);
console.log(sentence2);

//User template literals to add a new sentence in our sentence 3 variable:

let sentence3 = `${string6} ${string7} Bootcamp`
console.log(sentence3);

console.log(`${ 5 + 10}`);

let person = {
	name: "Michael",
	position: "developer",
	income: 50000,
	expenses: 60000
}

console.log(`${person.name} is a ${person.position}`);
console.log(`His income is ${person.income} and expenses at ${person.expenses}.
	His current balance is ${person.income - person.expenses}`);


//Destructuring Arrays and Objects
//Destructuring will allow us to save Array items or Object properties into new variable without having to create/initialize with accessing the items/properties one by one

let array1 = ["Curry", "Lillard", "Paul", "Irving"];

// let player1 = array1[0];
// let player2 = array1[1];
// let player3 = array1[2];
// let player4 = array1[3];
// console.log(player1, player2, player3, player4);

//Array destructuring is when we save array items into variables.

let [player1, player2, player3, player4] = array1;

console.log(player1, player2, player3, player4);

let array2 = ["Jokic", "Embiid", "Howard", "Anthony-Towns"];

//Get and save all items into variables except for Howard
let [center1, center2,, center4] = array2;

console.log(center4);

let pokemon1 = {

	name: "Balbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf", "Tackle", "Leech Seed"]

}

//Object Destructuring
let {level, type, name, moves, personality} = pokemon1;

console.log(level);
console.log(type);
console.log(name);
console.log(moves);
console.log(personality);

let pokemon2 = {

	name: "Charmander",
	type: "Fire",
	level: 11,
	moves: ["Ember", "Scratch"]

}

const {name: name2} = pokemon2;
console.log(name2);


// let {moves, name, type, level} = pokemon2;

// console.log(moves)

// function displayMsg() => {
// 	console.log(`Hello From Arrow`);
// }

const hello = () => {
	console.log(`Hello from arrow`);
}

hello()

//Arrow function with Parameters
const greet = (friend) =>{

	//console.log(friend)
	console.log(`Hi! ${friend.name}`)
}

greet(person);

//Arrow vs Traditional Function

//Implicit return - allows us to return a value from an arrow function without the use of return keyword.

//traditional addNum() function

/*function addNum(num1, num2){

	// console.log(num1, num2);

	// let result = num1 + num2;
	// return result;

	return num1 + num2
}*/

/*let subNum = (num1, num2) => num1 - num2;
let difference = subNum(50,5);

console.log(difference);
*/
//Implicit return, we return values without the return keyword.
/*
let subNum = (num1, num2){

}
*/


const addNum = (num1, num2) => num1 + num2;

let sum = addNum(50, 70);
console.log(sum)

//Traditional function vs Arrow function as Methods

let character1 = {
	name: "Cloud Strife",
	occupation: "Soldier",
	greet: function(){

		//In a traditional function as a method:
		//this keyword refers to the current object where the method is.
		console.log(this);
		console.log(`Hi! I'm ${this.name}`)
	},
	introduceJob: () => {
		//In an arrow function as method,
		//this keyword will not refer to the current object. Instead, it will refer to the global window object.
		console.log(this)
	}
}

character1.greet();
character1.introduceJob()


//Class Based Objects Blueprints
	//In Javascript, Classes are templates of objects.
	//We can create objects out of the use of classes.
	//Before the introduction of Classes in JS, we mimic this behavior of being able to create objects of the same blueprint using constructor function

/*	function Pokemon(name, type, level){

		this.name = name;
		this.type = type;
		this.level = level;

	}

	let poke1 = new Pokemon("Pika", "Graass", 12);
	console.log(poke1)*/

	//With the advent of ES6, we are now introduced to a special method of creating and initializing an object.
	//Classes - PascalCase
	//Normal func = camelcase
	class Car {
		constructor(brand, name, year){

			this.brand = brand;
			this.name = name;
			this.year = year;

		}
	}


	let car1 = new Car("Toyota", "Vios", "2001")
	let car2 = new Car("Cooper", "Mini", "2001")
	let car3 = new Car("Porsche", "911", "2001")

	console.log(car1)
	console.log(car2)
	console.log(car3)


	class Pokemon{
		constructor(name, type, level){

			this.name = name;
			this.type = type;
			this.level = level;
		}
	}

	let Onyx = new Pokemon("Onyx", "Rock/Ground", 18);
	let Jinx = new Pokemon("Jinx", "Psyhic/Ice", 24);
	let Muck = new Pokemon("Muck", "Poison", 14);

	console.log(Onyx);
	console.log(Jinx);
	console.log(Muck);